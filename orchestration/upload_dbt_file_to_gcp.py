import logging
import os
import sys
from os import environ as env
from google.cloud import storage
from google.oauth2 import service_account
from yaml import load, safe_load, YAMLError, FullLoader


def get_file_name(config_name):
    if config_name == "freshness":
        return "target/sources.json"
    elif config_name == "manifest":
        return "target/manifest.json"
    elif config_name == "catalog":
        return "target/catalog.json"
    else:
        return "target/run_results.json"


def upload_blob(storage_client, bucket_name, source_file_name, destination_blob_name):
    """Uploads a file to the bucket."""
    # The ID of your GCP project
    # If you don't specify credentials when constructing the client, the
    # client library will look for credentials in the environment.

    destination_blob_name = source_file_name.replace("target", "dbt")
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)

    blob.upload_from_filename(source_file_name)

    print(
        f"File {source_file_name} uploaded to {destination_blob_name} in bucket {bucket_name}."
    )


if __name__ == "__main__":
    config_name = sys.argv[1]
    file_name = get_file_name(config_name)
    config_dict = env.copy()

    # Get the gcloud storage client and authenticate
    scope = ["https://www.googleapis.com/auth/cloud-platform"]
    keyfile = load(env["GCP_SERVICE_CREDS"], Loader=FullLoader)
    credentials = service_account.Credentials.from_service_account_info(keyfile)
    scoped_credentials = credentials.with_scopes(scope)
    storage_client = storage.Client(credentials=scoped_credentials)

    if os.path.exists(file_name):
        upload_blob(storage_client, "atlan-vcluster-gitlab01", file_name, file_name)

    else:
        logging.error(
            f"Dbt File {file_name} is missing. Check if dbt run completed successfully"
        )
