"""
Run Zip API export.

The DAG can run backfill from a default start date when run with the parameter is_full_refresh=True.
"""

from datetime import datetime, timedelta

from airflow import DAG
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.models.param import Param
from airflow_utils import (
    DATA_IMAGE_3_10,
    clone_and_setup_extraction_cmd,
    gitlab_defaults,
    gitlab_pod_env_vars,
    slack_failed_task,
)
from kube_secrets import (
    ZIP_SANDBOX_API_KEY,
    SNOWFLAKE_ACCOUNT,
    SNOWFLAKE_LOAD_PASSWORD,
    SNOWFLAKE_LOAD_ROLE,
    SNOWFLAKE_LOAD_USER,
    SNOWFLAKE_LOAD_WAREHOUSE,
)
from kubernetes_helpers import get_affinity, get_toleration


pod_env_vars = {**gitlab_pod_env_vars, **{}}

params = {
    "is_full_refresh": Param(
        False,
        type="boolean",
    )
}

default_args = {
    "depends_on_past": False,
    "on_failure_callback": slack_failed_task,
    "owner": "airflow",
    "retries": 0,
    "retry_delay": timedelta(minutes=1),
    "sla": timedelta(hours=24),
    "sla_miss_callback": slack_failed_task,
}

dag = DAG(
    "el_zip_sandbox_extract",
    description="Extract data from Zip Sandbox API",
    default_args=default_args,
    # Run shortly before dbt dag which is at 8:45UTC
    schedule_interval="0 7 * * *",
    start_date=datetime(2025, 1, 1),
    catchup=False,
    max_active_runs=1,
    concurrency=2,
    params=params,
)

extraction_command_path = "python zip_sandbox/src/entrypoint_zip_sandbox_extract.py"

zip_sandbox_requests_extract_command = (
    f"{clone_and_setup_extraction_cmd} && " f"{extraction_command_path} requests"
)

zip_sandbox_vendors_extract_command = (
    f"{clone_and_setup_extraction_cmd} && " f"{extraction_command_path} vendors"
)

zip_sandbox_agreements_extract_command = (
    f"{clone_and_setup_extraction_cmd} && " f"{extraction_command_path} agreements"
)

zip_sandbox_task_name = "zip_sandbox_extract"

zip_sandbox_requests_task = KubernetesPodOperator(
    **gitlab_defaults,
    image=DATA_IMAGE_3_10,
    task_id="zip_sandbox_requests_extract",
    name="zip_sandbox_requests_extract",
    secrets=[
        SNOWFLAKE_ACCOUNT,
        SNOWFLAKE_LOAD_ROLE,
        SNOWFLAKE_LOAD_USER,
        SNOWFLAKE_LOAD_WAREHOUSE,
        SNOWFLAKE_LOAD_PASSWORD,
        ZIP_SANDBOX_API_KEY,
    ],
    env_vars={
        **pod_env_vars,
        "is_full_refresh": "{{params.is_full_refresh}}",
        "data_interval_start": "{{data_interval_start}}",
        "data_interval_end": "{{data_interval_end}}",
    },
    affinity=get_affinity("extraction"),
    tolerations=get_toleration("extraction"),
    arguments=[zip_sandbox_requests_extract_command],
    dag=dag,
)

zip_sandbox_vendors_task = KubernetesPodOperator(
    **gitlab_defaults,
    image=DATA_IMAGE_3_10,
    task_id="zip_sandbox_vendors_extract",
    name="zip_sandbox_vendors_extract",
    secrets=[
        SNOWFLAKE_ACCOUNT,
        SNOWFLAKE_LOAD_ROLE,
        SNOWFLAKE_LOAD_USER,
        SNOWFLAKE_LOAD_WAREHOUSE,
        SNOWFLAKE_LOAD_PASSWORD,
        ZIP_SANDBOX_API_KEY,
    ],
    env_vars={
        **pod_env_vars,
        "is_full_refresh": "{{params.is_full_refresh}}",
        "data_interval_start": "{{data_interval_start}}",
        "data_interval_end": "{{data_interval_end}}",
    },
    affinity=get_affinity("extraction"),
    tolerations=get_toleration("extraction"),
    arguments=[zip_sandbox_vendors_extract_command],
    dag=dag,
)

zip_sandbox_agreements_task = KubernetesPodOperator(
    **gitlab_defaults,
    image=DATA_IMAGE_3_10,
    task_id="zip_sandbox_agreements_extract",
    name="zip_sandbox_agreements_extract",
    secrets=[
        SNOWFLAKE_ACCOUNT,
        SNOWFLAKE_LOAD_ROLE,
        SNOWFLAKE_LOAD_USER,
        SNOWFLAKE_LOAD_WAREHOUSE,
        SNOWFLAKE_LOAD_PASSWORD,
        ZIP_SANDBOX_API_KEY,
    ],
    env_vars={
        **pod_env_vars,
        "is_full_refresh": "{{params.is_full_refresh}}",
        "data_interval_start": "{{data_interval_start}}",
        "data_interval_end": "{{data_interval_end}}",
    },
    affinity=get_affinity("extraction"),
    tolerations=get_toleration("extraction"),
    arguments=[zip_sandbox_agreements_extract_command],
    dag=dag,
)

zip_sandbox_requests_task
zip_sandbox_agreements_task
zip_sandbox_vendors_task
