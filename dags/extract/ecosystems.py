"""
ecosystems Extract

"""
import os
from datetime import datetime, timedelta

from airflow import DAG
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow_utils import (
    DATA_IMAGE_3_10,
    clone_and_setup_extraction_cmd,
    gitlab_defaults,
    gitlab_pod_env_vars,
    slack_failed_task,
)
from kube_secrets import (
    ECOSYSTEMS_API_KEY,
    SNOWFLAKE_ACCOUNT,
    SNOWFLAKE_LOAD_PASSWORD,
    SNOWFLAKE_LOAD_ROLE,
    SNOWFLAKE_LOAD_USER,
    SNOWFLAKE_LOAD_WAREHOUSE,
)
from kubernetes_helpers import get_affinity, get_toleration

env = os.environ.copy()
pod_env_vars = {**gitlab_pod_env_vars, **{}}

default_args = {
    "depends_on_past": False,
    "on_failure_callback": slack_failed_task,
    "owner": "airflow",
    "retries": 2,
    "retry_delay": timedelta(minutes=1),
    "sla": timedelta(hours=24),
    "sla_miss_callback": slack_failed_task,
    "start_date": datetime(2025, 3, 3),
}

dag = DAG(
    "ecosystems_extract",
    default_args=default_args,
    schedule_interval="0 0 * * *",
    concurrency=1,
    catchup=True,
)

ecosystems_extract_command = (
    f"{clone_and_setup_extraction_cmd} && " f"python ecosystems/src/execute.py"
)

ecosystems_operator = KubernetesPodOperator(
    **gitlab_defaults,
    image=DATA_IMAGE_3_10,
    task_id="ecosystems-extract",
    name="ecosystems-extract",
    secrets=[
        SNOWFLAKE_ACCOUNT,
        SNOWFLAKE_LOAD_ROLE,
        SNOWFLAKE_LOAD_USER,
        SNOWFLAKE_LOAD_WAREHOUSE,
        SNOWFLAKE_LOAD_PASSWORD,
        ECOSYSTEMS_API_KEY,
    ],
    env_vars={
        **pod_env_vars,
        "START_TIME": "{{ logical_date }}",
        "END_TIME": "{{ next_execution_date }}",
    },
    affinity=get_affinity("extraction"),
    tolerations=get_toleration("extraction"),
    arguments=[ecosystems_extract_command],
    dag=dag,
)
