"""
Utils module for EcoSystems
"""
import json
from dataclasses import dataclass
from os import environ as env

import requests


@dataclass
class Config:
    """
    Configuration class
    """

    endpoints = [
        {
            "name": "document",
            "api_url": "document/",
            "params": "createDateStart=%s&createDateEnd=%s&offset=%s&limit=%s",
            "table_name": "DOCUMENT",
        },
        {"name": "cva", "api_url": "vivien/cva?all=true", "table_name": "CVA"},
    ]
    base_url = "https://doclib.ecosystems.us/api/v1/"
    encoding = "utf-8"
    schema_name: str = "ecosystems"
    backfill_hours = 24
    overlapping_hours = 2


class JsonUtils:
    """
    Utils class for json transformation and filtering
    """

    headers = {"Authorization": env.get("ECOSYSTEMS_API_KEY")}

    def __init__(self):
        pass

    def get_response(self, url: str) -> requests.Response:
        """
        get response from the server
        """
        try:
            response = requests.get(url=url, headers=self.headers, timeout=120)
            response.raise_for_status()

            return response
        except requests.ConnectionError as e:
            raise ConnectionError("Error requesting job") from e

    @staticmethod
    def convert_response_to_json(response: requests.Response):
        """
        Convert Response object to json
        """
        return json.loads(response.text)

    def get_response_as_dict(self, url):
        """
        get prepared response in json format
        """
        response = self.get_response(url=url)

        return self.convert_response_to_json(response=response)
