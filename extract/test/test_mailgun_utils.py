"""
Test the MailgunUtils class
"""

from unittest.mock import patch

import pytest
from extract.mailgun.src.mailgun_utils import Config, JsonUtils


@pytest.fixture(autouse=True, name="json_utils")
def get_json_utils():
    """
    object of the class JsonUtils
    """
    return JsonUtils


def test_config_initialization():
    """
    Test config_initialization
    """
    config = Config(
        api_key="test_key",
        domains=["test.com"],
        base_url="https://test.com",
        request_timeout=60,
        encoding="utf-8",
    )
    assert config.api_key == "test_key"
    assert config.domains == ["test.com"]
    assert config.base_url == "https://test.com"
    assert config.request_timeout == 60


@pytest.mark.parametrize(
    "env_api_key, expected_api_key",
    [
        ("test_env_key", "test_env_key"),
        (None, None),
        ("test_env_key", "test_env_key"),
    ],
)
def test_config_from_env(env_api_key, expected_api_key):
    """
    Test config_from_env
    """
    with patch.dict(
        "os.environ", {"MAILGUN_API_KEY": env_api_key} if env_api_key else {}
    ):
        config = Config.from_env()
        assert config.api_key == expected_api_key
        assert config.domains == [
            "customers.gitlab.com",
            "mg.release.gitlab.net",
            "gitlab.io",
            "users.noreply.gitlab.com",
        ]
        assert config.base_url == "https://api.mailgun.net/v3"
        assert config.request_timeout == 120


@pytest.mark.parametrize(
    "input_data, expected_output",
    [
        ({"a": 1, "b": {"c": 2, "d": {"e": 3}}}, {"a": 1, "b_c": 2, "b_d_e": 3}),
        ({"x": [1, 2, {"y": 3}]}, {"x_0": 1, "x_1": 2, "x_2_y": 3}),
        (
            {"p": "test", "q": {"r": [{"s": 1}, {"t": 2}]}},
            {"p": "test", "q_r_0_s": 1, "q_r_1_t": 2},
        ),
        ({}, {}),
    ],
)
def test_flatten_json(input_data, expected_output, json_utils):
    """
    Test the flatten_json function with various input scenarios:
    - Nested dictionary
    - Dictionary with list
    - Complex nested structure
    - Empty dictionary
    """
    result = json_utils.flatten_json(input_data)
    assert result == expected_output


@pytest.mark.parametrize(
    "input_data, enrichment_data, expected_output",
    [
        (
            {"event": "delivered", "recipient": "user@example.com"},
            ("domain", "example.com"),
            {
                "event": "delivered",
                "recipient": "user@example.com",
                "domain": "example.com",
            },
        ),
        (
            {"message_id": "123", "timestamp": 1234567890},
            ("source", "mailgun"),
            {"message_id": "123", "timestamp": 1234567890, "source": "mailgun"},
        ),
        ({}, ("empty_field", None), {"empty_field": None}),
    ],
)
def test_enrich_json(input_data, enrichment_data, expected_output, json_utils):
    """
    Test the enrich_json function with various input scenarios:
    - Normal case with existing data
    - Adding a new field to existing data
    - Adding a field to an empty dictionary
    """
    result = json_utils.enrich_json(input_data, enrichment_data)
    assert result == expected_output


@pytest.mark.parametrize(
    "invalid_input, invalid_enrichment",
    [
        ("not a dict", ("key", "value")),
        ({"key": "value"}, "not a tuple"),
        ({"key": "value"}, ("key",)),
        ({"key": "value"}, ("key", "value", "extra")),
    ],
)
def test_enrich_json_invalid_inputs(invalid_input, invalid_enrichment, json_utils):
    """
    Test the enrich_json function with invalid inputs to ensure it raises appropriate exceptions
    """
    with pytest.raises((TypeError, ValueError)):
        json_utils.enrich_json(invalid_input, invalid_enrichment)


@pytest.mark.parametrize(
    "input_data, expected_output",
    [
        (
            {
                "id": "123",
                "envelope_sender": "sender@test.com",
                "message_headers_from": "from@test.com",
                "event": "delivered",
                "timestamp": 1234567890,
                "extra_field": "should_be_filtered",
            },
            {
                "id": "123",
                "envelope_sender": "sender@test.com",
                "message_headers_from": "from@test.com",
                "event": "delivered",
                "timestamp": 1234567890,
            },
        ),
        (
            {
                "delivery-status_code": 200,
                "delivery-status_mx-host": "mx.test.com",
                "geolocation_country": "US",
                "non_existing_field": "value",
            },
            {
                "delivery-status_code": 200,
                "delivery-status_mx-host": "mx.test.com",
                "geolocation_country": "US",
            },
        ),
        ({}, {}),  # Test with empty dictionary
        (
            {"unknown_field": "value", "another_field": "test"},
            {},  # Should return empty dict when no fields match filter
        ),
    ],
)
def test_filter_columns(input_data, expected_output, json_utils):
    """
    Test the filter_columns function with various input scenarios:
    - Dictionary with mix of allowed and filtered fields
    - Dictionary with delivery status and geolocation fields
    - Empty dictionary
    - Dictionary with no matching fields
    """
    result = json_utils.filter_columns(input_data)
    assert result == expected_output


# create test case for jsonutils function filter_rows
@pytest.mark.parametrize(
    "input_data, expected_output",
    [
        (
            {"message_headers_subject": "Managing users in your subscription"},
            True,  # Should filter out this message
        ),
        (
            {
                "message_headers_subject": "Additional charges for your GitLab subscription"
            },
            True,  # Should filter out this message
        ),
        (
            {"message_headers_subject": "Your GitLab subscription has been reconciled"},
            True,  # Should filter out this message
        ),
        (
            {"message_headers_subject": "Different subject line"},
            False,  # Should not filter out this message
        ),
        (
            {"message_headers_subject": None},
            False,  # Should not filter when subject is None
        ),
        (
            {},  # Empty dictionary
            False,  # Should not filter when subject field is missing
        ),
    ],
)
def test_filter_rows(input_data, expected_output, json_utils):
    """
    Test the filter_rows function with various input scenarios:
    - Messages that should be filtered out
    - Messages that should not be filtered
    - Edge cases with missing or None values
    """
    result = json_utils.filter_rows_per_subject(input_data)
    assert result == expected_output
