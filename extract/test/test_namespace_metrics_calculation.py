"""
The main test routine for instance_namespace_metrics
"""

import os
from datetime import datetime, timedelta
from unittest import mock

import pytest
from extract.saas_usage_ping.namespace_metrics_calculation import (
    NamespaceMetricsCalculation,
)


@pytest.fixture(autouse=True, name="set_env_variables")
def mock_settings_env_vars():
    """
    Simulate OS env. variables
    """
    with mock.patch.dict(
        os.environ,
        {"GITLAB_ANALYTICS_PRIVATE_TOKEN": "xxx", "SNOWFLAKE_PROD_DATABASE": "PROD"},
    ):
        yield


@pytest.fixture(
    name="metrics_calculation",
)
def get_metrics_calculation(set_env_variables):
    """
    Return NamespacePing object
    """
    _ = set_env_variables
    metrics_calculation = NamespaceMetricsCalculation()
    metrics_calculation.metrics_to_backfill = ["metric1", "metric3"]
    metrics_calculation.end_date = datetime.now()
    metrics_calculation.start_date_28 = metrics_calculation.end_date - timedelta(
        days=28
    )

    return metrics_calculation


@pytest.fixture(name="queries")
def get_usage_ping_queries(metrics_calculation):
    """
    Fixture for namespace file
    """

    return metrics_calculation.read_namespace_queries(
        file_name="metrics_and_queries/database_namespace_queries.json"
    )


def test_metrics_calculation_class(metrics_calculation):
    """
    Check class creation
    """
    assert metrics_calculation


@pytest.mark.parametrize(
    "test_value, expected_value",
    [
        ("active_user_count", False),
        (
            "usage_activity_by_stage_monthly.manage.groups_with_event_streaming_destinations",
            True,
        ),
        ("usage_activity_by_stage_monthly.manage.audit_event_destinations", True),
        ("counts.boards", False),
        ("usage_activity_by_stage_monthly.configure.instance_clusters_enabled", True),
        ("counts_monthly.deployments", True),
    ],
)
def test_get_backfill_filter(metrics_calculation, queries, test_value, expected_value):
    """
    test backfill filter accuracy with
    lambda as a return statement
    """

    metrics_filter = metrics_calculation.filter_metrics([test_value])

    for namespace in queries:
        if metrics_filter(namespace):
            assert namespace.get("time_window_query") == expected_value
            assert expected_value is True
            assert namespace.get("counter_name") == test_value


def test_json_file_consistency_time_window_query(queries):
    """
    Test is dictionary is constructed properly in
    the file usage_ping_namespace_queries.json

    If time_window_query=True,
    counter_query should contain ["between_start_date","between_end_date"]
    """

    for metrics in queries:
        counter_query = metrics.get("counter_query")
        time_window_query = bool(metrics.get("time_window_query", False))

        time_window_yes = (
            "between_start_date" in counter_query
            and "between_end_date" in counter_query
            and time_window_query is True
        )
        time_window_no = (
            "between_start_date" not in counter_query
            and "between_end_date" not in counter_query
            and time_window_query is False
        )

        assert time_window_yes or time_window_no


def test_queries(queries):
    """
    Test file loading
    """

    assert queries


def test_queries_error(metrics_calculation):
    """
    Test file loading
    """
    with pytest.raises(FileNotFoundError):
        metrics_calculation.read_namespace_queries(file_name="THIS_DOES_NOT_EXITS.json")


def test_json_file_consistency_level(queries):
    """
    Test is dictionary is constructed properly in
    the file usage_ping_namespace_queries.json

    If level=namespace
    """

    for metrics in queries:
        level = metrics.get("level")

        assert level == "namespace"


def test_replace_placeholders(metrics_calculation):
    """
    Test string replace for query
    """
    sql = "SELECT 1 FROM TABLE WHERE created_at BETWEEN between_start_date AND between_end_date"

    actual = metrics_calculation.replace_placeholders(sql=sql)

    assert "between_start_date" not in actual
    assert "between_end_date" not in actual

    assert datetime.strftime(metrics_calculation.end_date, "%Y-%m-%d") in actual
    assert datetime.strftime(metrics_calculation.start_date_28, "%Y-%m-%d") in actual


def test_validate_namespace_queries(queries):
    """
    Test does namespace file have proper SQL
    % -> %%
    """

    for n in queries:
        if "%" in n.get("counter_query", ""):
            assert "%%" in n.get("counter_query", "")


def test_prepare_insert_query(queries, metrics_calculation):
    """
    Test query replacement
    """
    insert_part = (
        "(id, namespace_ultimate_parent_id, counter_value, "
        "ping_name, level, query_ran, error, ping_date, _uploaded_at)"
    )
    for namespace_query in queries:
        expected = metrics_calculation.insert_query(
            query_dict=namespace_query.get("counter_query"),
            ping_name=namespace_query.get("counter_name"),
            ping_date=metrics_calculation.end_date,
        )
        assert insert_part in expected
        assert "Success" in expected
        assert "namespace" in expected
        assert "FROM" in expected
        assert expected.count("INSERT") == 1


@pytest.mark.parametrize(
    "test, expected",
    [
        ((1, 20), (0, 30)),
        ((2, 20), (30, 60)),
        ((3, 20), (60, 90)),
        ((20, 20), (570, 600)),
        ((1, 19), (0, 31)),
        ((2, 19), (31, 62)),
    ],
)
def test_chunk_list(metrics_calculation, test, expected):
    """
    Test list slicing
    for dynamic queries splitting
    """
    metrics_calculation.chunk_no = test[0]
    metrics_calculation.number_of_tasks = test[1]

    actual = metrics_calculation.chunks(namespace_size=582)

    assert actual == expected


def test_chunk_list_edge_case(queries, metrics_calculation):
    """
    Test list slicing
    for dynamic queries splitting
    """
    metrics_calculation.chunk_no = 0
    metrics_calculation.number_of_tasks = 0

    actual = metrics_calculation.chunks(namespace_size=len(queries))

    assert actual == (0, len(queries))


def test_generate_error_message(metrics_calculation):
    """
    Test generate_error_message
    """

    error_message = """(snowflake.connector.errors.ProgrammingError) 001003 (42000): 01ae51f0-0406-8c1e-0000-289d5a74c882: SQL compilation error:\n'
[2023-08-15 08:16:59,539] INFO - b"syntax error line 1 at position 343 unexpected '{'.\n
[2023-08-15 08:16:59,539] INFO - b"syntax error line 1 at position 342 unexpected '.'.\n"""

    expected = metrics_calculation.error_message(input_error=error_message)

    assert "\n" not in expected
    assert "'" not in expected


def test_generate_error_message_sql(metrics_calculation):
    """
    Test generate_error_message
    """

    error_message = "(snowflake.connector.errors.ProgrammingError) 001003 (42000): 01ae51f0-0406-8c1e-0000-289d5a74c882  This is remaining part[SQL: This will be deleted"

    expected = metrics_calculation.error_message(input_error=error_message)

    assert "[SQL:" not in expected
    assert "This will be deleted" not in expected
    assert "This is remaining part" in expected


def test_generate_error_insert(metrics_calculation):
    """
    Test generate_error_insert
    """
    metrics_sql_select = "SELECT 1"
    expected = metrics_calculation.error_query(
        metrics_name="test_metrics",
        metrics_level="namespace",
        metric_sql_select=metrics_sql_select,
        error_text="Test error message",
    )

    assert metrics_calculation.SQL_INSERT_PART in expected
    assert metrics_sql_select in expected
    assert "namespace" in expected
    assert "NULL, NULL, NULL" in expected
    assert "\\" not in expected


def test_multiple_select_statement(queries, metrics_calculation):
    """
    Test option when we have 2 SELECT statement (subquery),
    as it was failing before.
    Example:
    Metrics: counts.service_desk_issues
    SQL (have 2 SELECT statement):
        SELECT
          namespaces_xf.namespace_ultimate_parent_id  AS id,
          namespaces_xf.namespace_ultimate_parent_id,
          COUNT(issues.id)                            AS counter_value
        FROM prep.gitlab_dotcom.gitlab_dotcom_issues_dedupe_source AS issues
        LEFT JOIN prep.gitlab_dotcom.gitlab_dotcom_projects_dedupe_source AS projects
          ON projects.id = issues.project_id
        LEFT JOIN prep.gitlab_dotcom.gitlab_dotcom_namespaces_dedupe_source AS namespaces
          ON namespaces.id = projects.namespace_id
        LEFT JOIN prod.legacy.gitlab_dotcom_namespaces_xf AS namespaces_xf
          ON namespaces.id = namespaces_xf.namespace_id
        WHERE issues.project_id IN
          (
            SELECT projects.id
            FROM prep.gitlab_dotcom.gitlab_dotcom_projects_dedupe_source AS projects
            WHERE projects.service_desk_enabled = TRUE
          )
            AND issues.author_id = 1257257
            AND issues.confidential = TRUE
        GROUP BY 1

    """
    for metrics in queries:
        actual = metrics_calculation.insert_query(
            query_dict=metrics.get("counter_query"),
            ping_name=metrics.get("counter_name"),
            ping_date=metrics_calculation.end_date,
        )

        assert actual.count("INSERT INTO") == 1
        assert actual.count("DATE_PART(epoch_second, CURRENT_TIMESTAMP())") == 1
        assert actual.count("Success") == 1
        assert actual.count(metrics.get("counter_name")) == 1


@pytest.mark.parametrize(
    "queries, expected_count, run_backfill",
    [
        (
            [
                {"time_window_query": True, "counter_name": "metric1"},
                {"time_window_query": False, "counter_name": "metric2"},
                {"time_window_query": True, "counter_name": "metric3"},
            ],
            3,
            False,
        ),
        (
            [
                {"time_window_query": True, "counter_name": "metric1"},
                {"time_window_query": False, "counter_name": "metric2"},
                {"time_window_query": True, "counter_name": "metric3"},
            ],
            3,
            False,
        ),
        (
            [],
            0,
            False,
        ),
        (
            [
                {"time_window_query": True, "counter_name": "metric1"},
                {"time_window_query": False, "counter_name": "metric2"},
                {"time_window_query": True, "counter_name": "metric3"},
            ],
            3,
            False,
        ),
    ],
)
def test_get_query_count(metrics_calculation, queries, expected_count, run_backfill):
    """
    Test the get_metrics_no function with various scenarios
    """
    assert (
        metrics_calculation.get_query_count(queries, run_backfill=run_backfill)
        == expected_count
    )


@pytest.mark.parametrize(
    "original_queries, expected_result",
    [
        ([{"time_window_query": True, "counter_name": "metric1"}], 1),
        ([{"time_window_query": False, "counter_name": "metric1"}], 0),
        ([{"time_window_query": True, "counter_name": "metric2"}], 0),
        (
            [
                {"time_window_query": True, "counter_name": "metric1"},
                {"time_window_query": True, "counter_name": "metric3"},
            ],
            2,
        ),
        ([{"time_window_query": False, "counter_name": "metric3"}], 0),
        ([{"time_window_query": True, "counter_name": "metric4"}], 0),
    ],
)
def test_filter_backfill_queries(
    metrics_calculation, original_queries, expected_result
):
    """
    Test the filter_backfill_queries function
    The only queries to remain should be based off the metrics declared here:
    metrics_calculation.metrics_to_backfill = ["metric1", "metric3"]
    """
    filtered_backfill_queries = metrics_calculation.filter_backfill_queries(
        original_queries
    )

    assert (
        len(filtered_backfill_queries) == expected_result
    ), f"Failed for original_queries: {original_queries}"


def test_get_namespace_queries_without_backfill(metrics_calculation):
    # Mock dependencies
    metrics_calculation.read_namespace_queries = mock.MagicMock(
        return_value=["query1", "query2", "query3"]
    )
    metrics_calculation.utils = mock.MagicMock()
    metrics_calculation.utils.config = {
        "some_metrics_type": {"source_file": "some_file.yml"}
    }
    metrics_calculation.metrics_type = "some_metrics_type"
    metrics_calculation.chunks = mock.MagicMock(return_value=(0, 3))

    result = metrics_calculation.get_namespace_queries(run_backfill=False)

    # Assertions
    metrics_calculation.read_namespace_queries.assert_called_once_with(
        file_name="some_file.yml"
    )
    metrics_calculation.chunks.assert_called_once_with(namespace_size=3)
    assert result == ["query1", "query2", "query3"]
