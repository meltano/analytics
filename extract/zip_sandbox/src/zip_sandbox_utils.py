"""
Utility methods for zip sandbox extract
"""

import json
import os
from datetime import datetime, timezone
from logging import error, info
from time import sleep
from typing import Any, Dict, List, Tuple, Union

import requests
from gitlabdata.orchestration_utils import (
    snowflake_engine_factory,
    snowflake_stage_load_copy_remove,
)

config_dict = os.environ.copy()

# Sandbox DAG
ZIP_SANDBOX_API_KEY = config_dict.get("ZIP_SANDBOX_API_KEY")

HEADERS = {"Accept": "application/json", "Zip-Api-Key": f"{ZIP_SANDBOX_API_KEY}"}

BASE_URL = "https://api.ziphq.com/"

TIMEOUT = 60


def paginated_api_call(
    params: Dict[str, Union[int, str]], table_name: str
) -> List[Dict]:
    """
    Make paginated API calls and return the combined results.
    """
    results = []
    page = 1
    while True:
        response = get_response(params, page, table_name)
        if response.status_code != 200:
            break

        response_list, next_page_token = extract_json_and_next_page_token(response)
        results.extend(response_list)

        if next_page_token is None:
            info(f"Extracted all {table_name}")
            break

        params["page_token"] = next_page_token
        page += 1

    return results


def get_start_and_end_date() -> Tuple[int, int]:
    """
    Get the start and end date and convert to epoch.
    """
    is_full_refresh = os.environ["is_full_refresh"]
    if is_full_refresh.lower() == "true":
        start_date = datetime(
            2023, 2, 1, tzinfo=timezone.utc
        )  # This is the date when zip went live
    else:
        data_interval_start = os.environ["data_interval_start"]
        start_date = datetime.strptime(data_interval_start, "%Y-%m-%dT%H:%M:%S%z")

    data_interval_end = os.environ["data_interval_end"]
    end_date = datetime.strptime(data_interval_end, "%Y-%m-%dT%H:%M:%S%z")

    info(f"Start date: {start_date}")
    info(f"End date: {end_date}")
    return int(start_date.timestamp()), int(end_date.timestamp())


def upload_events_to_snowflake(events: list, table_name: str) -> None:
    """Upload event payload to Snowflake"""
    upload_dict = {"data": events}

    schema_name = "zip_sandbox"
    stage_name = "zip_load_stage"
    json_dump_filename = f"zip_sandbox_{table_name}.json"
    upload_payload_to_snowflake(
        upload_dict, schema_name, stage_name, table_name, json_dump_filename
    )


def upload_payload_to_snowflake(
    payload: Dict,
    schema_name: str,
    stage_name: str,
    table_name: str,
    json_dump_filename: str = "to_upload.json",
):
    """
    Upload payload to Snowflake using snowflake_stage_load_copy_remove()
    """
    info("Uploading payload to Snowflake")
    loader_engine = snowflake_engine_factory(config_dict, "LOADER")
    with open(json_dump_filename, "w+", encoding="utf8") as upload_file:
        json.dump(payload, upload_file)

    snowflake_stage_load_copy_remove(
        json_dump_filename,
        f"{schema_name}.{stage_name}",
        f"{schema_name}.{table_name}",
        loader_engine,
    )
    loader_engine.dispose()


def get_response(params, page, table_name) -> Any:

    """
    Get response from the Zip API.
    """
    info(f"Getting reports, extracting page {page}")
    try:
        response = requests.get(
            f"{BASE_URL}{table_name}",
            headers=HEADERS,
            params=params,
            timeout=TIMEOUT,
        )
        response.raise_for_status()
        return response
    except requests.RequestException as exp:
        if response.status_code == 429:
            info(
                f"Rate limit exceeded: {response.status_code}, waiting for 60 seconds before sending another request"
            )
            sleep(60)
        error(f"Error occurred: {exp}")
        return None


def extract_json_and_next_page_token(response) -> Any:
    """
    Extract json from response.
    """
    response_json = response.json()
    response_list = response_json["list"]
    # if the response contains "next_page_token" then paginate
    next_page_token = response_json.get("next_page_token")

    return response_list, next_page_token
