"""
Routines for internal events namespace Metrics
"""

import sys
from logging import basicConfig, info
from typing import Any, Dict, Optional

import yaml
from fire import Fire

from utils import Utils


class SQLGenerate:
    """
    This class is responsible for generating SQL statements based on provided metrics.
    It has a constructor to initialize the table name and a utility object.
    It has three methods: get_time_frame, get_event_list, and transform.
    The get_time_frame method returns a
    """

    def __init__(self, is_backfill: bool = False):
        """
        Initialize the SQLGenerate class with the table name and utility object.
        """
        self.util = Utils()
        self.database = (
            self.util.database
            if self.util.database == "PROD"
            else self.util.double_quoted(self.util.database)
        )
        self.schema = "common_mart_product"
        self.is_backfill = is_backfill

    @staticmethod
    def get_time_frame(time_frame: Optional[Dict[Any, Any]] = None) -> str:
        """
        Get time frame for specific SQL
        """
        res = ""

        if time_frame == "7d":
            res = "AND behavior_at BETWEEN DATEADD(DAY, -7, between_end_date) AND between_end_date "
        if time_frame == "28d":
            res = "AND behavior_at BETWEEN DATEADD(DAY, -28, between_end_date) AND between_end_date "
        return res

    def get_event_list(self, metrics: dict) -> str:
        """
        Get event list for specific SQL
        """
        events_list = []
        events, options = metrics.get("events"), metrics.get("options")
        res = ""

        if events:
            events_list.extend([event.get("name") for event in events])

        if options and options.get("events", None):
            events_list.extend(options.get("events"))

        if events_list:
            if len(events_list) == 1:
                res += f"AND redis_event_name = {self.util.quoted(events_list[0])} "
            else:
                res += f"AND redis_event_name IN ({','.join([self.util.quoted(event) for event in events_list])}) "
        return res

    @staticmethod
    def determine_count_type(metrics: dict) -> str:
        """

        Determine either (COUNT 1) or COUNT(DISTINCT gsc_pseudonymized_user_id)
        depends on the rules:
        * data_source: redis -> COUNT
        * data_source: redis_hll -> DISTINCT COUNT
        * data_source: internal_events just name -> COUNT (example: https://gitlab.com/gitlab-org/gitlab/blob/master/ee/config/metrics/counts_28d/count_total_dependency_proxy_packages_maven_file_pulled_from_external_monthly.yml#L18-19)
        * data_source: internal_events if they have "unique", -> DISTINCT COUNT (example: https://gitlab.com/gitlab-org/gitlab/blob/master/ee/config/metrics/counts_28d/20210310164247_g_project_management_epic_reopened_monthly.yml#L14)
        """
        res = "COUNT(1)"
        metric = metrics.get("data_source")
        event: Any = metrics.get("events")

        if metric == "redis":
            res = "COUNT(1)"

        if metric == "redis_hll":
            res = "COUNT(DISTINCT gsc_pseudonymized_user_id)"

        if metric == "internal_events":

            event_details = {} if event is None else event[0]

            if event_details.get("name") and not event_details.get("unique"):
                res = "COUNT(1)"

            if event_details.get("unique") == "user.id":
                res = "COUNT(DISTINCT gsc_pseudonymized_user_id)"

        return res

    def get_table_name(self, time_frame: str) -> str:
        """
        Get table name for specific time_frame
        """
        table_name_prefix = "mart_behavior_structured_event_service_ping_metrics"

        tables = {
            "7d": f"{table_name_prefix}_7d",
            "28d": f"{table_name_prefix}_28d",
            "all": f"{table_name_prefix}",
            "backfill": f"{table_name_prefix}",
        }

        return f"{self.database}.{self.schema}.{tables.get(time_frame, '')}"

    def get_additional_filters(self, metrics: dict) -> str:
        """
        Get additional filters for specific SQL
        1. for "all" metrics data_source = ['redis' | 'redis_hll' | 'internal_events'] AND time_frame='all'
        2. for backfilling data_source = ['redis' | 'redis_hll' | 'internal_events'] AND time_frame=['7d' | '28d' | 'all']
        """
        res = ""
        data_source = self.util.quoted(metrics.get("data_source", ""))
        time_frame = self.util.quoted(metrics.get("time_frame", ""))

        if self.is_backfill or metrics.get("time_frame") == "all":
            res = f"AND data_source={data_source} AND time_frame={time_frame} "

        return res

    def transform(self, metrics: dict) -> str:
        """
        Transform YML definition to SQL statement and template
        """

        # Generate SELECT part with columns
        table_name = self.get_table_name(
            "backfill" if self.is_backfill else metrics.get("time_frame", "")
        )

        res = (
            f"SELECT "
            f"ultimate_parent_namespace_id AS id, "
            f"ultimate_parent_namespace_id AS namespace_ultimate_parent_id, "
            f"{self.determine_count_type(metrics=metrics)} AS counter_value "
            f"FROM {table_name} "
        )

        # Generate WHERE clause for metrics
        res += f"WHERE metrics_path = {self.util.quoted(metrics.get('key_path'))} "
        # Generate list of event, if any
        res += self.get_event_list(metrics=metrics)
        # Generate time frame, if any
        res += self.get_time_frame(time_frame=metrics.get("time_frame"))
        # generate filters to improve speed of the execution:
        # 1. for "all" metrics data_source = ['redis' | 'redis_hll' | 'internal_events'] AND time_frame='all'
        # 2. for backfilling data_source = ['redis' | 'redis_hll' | 'internal_events'] AND time_frame=['7d' | '28d' | 'all']
        res += self.get_additional_filters(metrics=metrics)
        # Generate GROUP BY clause
        res += "GROUP BY ALL;"

        return res


class InternalEventsNamespaceMetrics:
    """
    Routines for internal events namespace metrics
    """

    def __init__(self):
        """
        Initialize the InternalNamespaceMetrics class.

        This method sets up the initial state of the InternalNamespaceMetrics object.
        """
        self.url = "https://gitlab.com/api/v4/usage_data/metric_definitions"
        self.data_source = ["redis", "redis_hll", "internal_events"]
        self.metrics_status = ["active"]
        self.util = Utils()
        self.util.headers = {}
        self.is_backfill: bool = False

    def get_sql_metrics_definition(self, metric: dict) -> dict:
        """
        Generate SQL template for internal events namespace metrics
        """
        template = {}
        sql = SQLGenerate(self.is_backfill)

        template["counter_name"] = metric.get("key_path")
        template["counter_query"] = sql.transform(metrics=metric)
        template["time_window_query"] = metric.get("time_frame") in ("7d", "28d")
        template["level"] = "namespace"
        template["time_frame"] = metric.get("time_frame")

        return template

    def generate_sql_metrics(self, metrics_data: dict) -> list:
        """
        Generate SQL metrics for internal events namespace metrics.
        """
        res = []
        for metric in metrics_data:
            res.append(self.get_sql_metrics_definition(metric=metric))

        return res

    def get_metric_names_to_keep(self) -> list:

        metric_names = self.util.load_from_yml_file(
            self.util.INTERNAL_EVENTS_METRICS_TO_KEEP_FILE
        )
        return metric_names

    def filter_metrics(self, metrics: dict) -> list:
        """
        Function to filter out the metrics we do not want to calculate
        """
        return [
            metric_dict
            for metric_dict in metrics
            if metric_dict.get("key_path") in self.get_metric_names_to_keep()
            and metric_dict.get("data_source") in self.data_source
            and metric_dict.get("status") in self.metrics_status
        ]

    def generate(self, is_backfill: bool = False):
        """
        1. Get metrics definition from API
        2. Transform YML to JSON
        3. Transform JSON to SQL
        4. Save SQL to file

        """
        info("Start generating internal events namespace metrics")
        self.is_backfill = is_backfill
        info(f"Backfill: {self.is_backfill}")

        metrics_raw = self.util.get_response(url=self.url)
        metrics_prepared = yaml.safe_load(metrics_raw.text)

        metrics_prepared = self.filter_metrics(metrics=metrics_prepared)
        metrics_prepared = self.generate_sql_metrics(metrics_data=metrics_prepared)

        self.util.save_to_json_file(
            file_name=self.util.config["internal_events_namespace_metrics"][
                "source_file"
            ],
            json_data=metrics_prepared,
        )

        info("End generating internal events namespace metrics")


if __name__ == "__main__":
    basicConfig(stream=sys.stdout, level=20)
    Fire(InternalEventsNamespaceMetrics)
