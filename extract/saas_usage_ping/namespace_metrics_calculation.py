"""
Namespace module for support instance_namespace_ping pipeline

namespace_metrics_calculation.py is responsible for uploading the following into Snowflake:
- usage ping namespace
- internal events namespace
"""

import datetime
import math
import os
import sys
from logging import basicConfig, info
from typing import List

from fire import Fire

from engine_factory import EngineFactory
from internal_events_namespace_metrics import InternalEventsNamespaceMetrics
from utils import Utils
from gitlabdata.orchestration_utils import execute_query_str


class NamespaceMetricsCalculation:
    """
    Handling NamespaceMetricsCalculation pipeline
    - usage ping namespace
    - internal events namespace
    """

    def __init__(
        self,
        ping_date=None,
        chunk_no=0,
        number_of_tasks=0,
        metrics_to_backfill=None,
        metrics_type="database_namespace_metrics",
    ):
        if ping_date is not None:
            self.end_date = datetime.datetime.strptime(ping_date, "%Y-%m-%d").date()
        else:
            self.end_date = datetime.datetime.now().date()

        self.start_date_28 = self.end_date - datetime.timedelta(28)

        if metrics_to_backfill is not None:
            self.metrics_to_backfill = metrics_to_backfill
        else:
            self.metrics_to_backfill = []

        self.metrics_type = metrics_type.replace("_backfill", "")

        # chunk_no = 0 - instance_namespace_metrics back filling (no chunks)
        # chunk_no > 0 - load instance_namespace_metrics in chunks
        self.chunk_no = chunk_no
        self.number_of_tasks = number_of_tasks

        self.engine_factory = EngineFactory()
        self.utils = Utils()

        self.SQL_INSERT_PART = (
            "INSERT INTO "
            f"{self.engine_factory.schema_name}.{self.utils.config[self.metrics_type]['table_name']}"
            "(id, "
            "namespace_ultimate_parent_id, "
            "counter_value, "
            "ping_name, "
            "level, "
            "query_ran, "
            "error, "
            "ping_date, "
            "_uploaded_at) "
        )

    @staticmethod
    def filter_metrics(filter_list: list):
        """
        Filter instance_namespace_metrics for
        processing a namespace metrics load
        """

        return (
            lambda namespace: namespace.get("time_window_query")
            and namespace.get("counter_name") in filter_list
        )

    def replace_placeholders(self, sql: str) -> str:
        """
        Replace dates placeholders with proper dates:
        Usage:
        Input: "SELECT 1 FROM TABLE WHERE created_at BETWEEN between_start_date AND between_end_date"
        Output: "SELECT 1 FROM TABLE WHERE created_at BETWEEN '2022-01-01' AND '2022-01-28'"
        """

        base_query = sql
        base_query = base_query.replace("between_end_date", f"'{str(self.end_date)}'")
        base_query = base_query.replace(
            "between_start_date", f"'{str(self.start_date_28)}'"
        )

        return base_query

    def get_prepared_values(self, query: dict) -> tuple:
        """
        Prepare variables for query
        """

        name = query.get("counter_name", "Missing Name")

        sql_raw = str(query.get("counter_query"))
        prepared_sql = self.replace_placeholders(sql_raw)

        level = query.get("level")

        return name, prepared_sql, level

    def insert_query(
        self,
        query_dict: str,
        ping_name: str,
        ping_date: str,
        ping_level: str = "namespace",
    ) -> str:
        """
        Prepare query for insert and enrich it with few more values
        """

        prepared_query = f"{self.SQL_INSERT_PART}{query_dict}"
        safe_dict = query_dict.replace("'", "\\'")
        prepared_query = prepared_query.replace(
            "FROM",
            f",'{ping_name}', '{ping_level}', '{safe_dict}', 'Success', '{ping_date}', DATE_PART(epoch_second, CURRENT_TIMESTAMP()) FROM",
            1,
        )

        return prepared_query

    @staticmethod
    def error_message(input_error: str) -> str:
        """
        Generate error message and delete characters
        Snowflake can't digest
        """

        res = input_error.replace("\n", " ").replace("'", "")

        if "[SQL:" in res:
            res = res[100 : res.index("[SQL:")]

        return res

    def error_query(
        self,
        metrics_name: str,
        metrics_level: str,
        metric_sql_select: str,
        error_text: str,
    ) -> str:
        """
        Refine and prepare data for uploading in case error occurred with
        instance_namespace_metrics query.

        This is workaround and Snowflake creates an error in case SQL contains single quote.
        This function sort it out
        """

        error_sql = metric_sql_select.replace("'", "\\'")

        error_record = (
            f"{self.SQL_INSERT_PART} "
            f"VALUES "
            f"(NULL, NULL, NULL, "
            f"'{metrics_name}', "
            f"'{metrics_level}', "
            f"'placeholder_error_sql', "
            f"'{error_text}', "
            f"'{self.end_date}', "
            f"DATE_PART(epoch_second, CURRENT_TIMESTAMP()))"
        ).replace("\\", "")

        error_record = error_record.replace("placeholder_error_sql", error_sql)

        info(f"......inserting ERROR record: {error_record}")
        return error_record

    def upload(self, query_dict: dict, conn) -> None:
        """
        Execute query and return results.
        This should be done directly in Snowflake
        """
        name, sql_select, level = self.get_prepared_values(query=query_dict)

        sql_ready = self.insert_query(
            query_dict=sql_select, ping_name=name, ping_date=self.end_date
        )

        try:
            execute_query_str(conn, sql_ready)
        except Exception as programming_error:
            error_message = self.error_message(input_error=str(programming_error))

            info(
                f"......ERROR: {type(programming_error).__name__}...."
                f"{error_message}"
            )

            insert_error_record = self.error_query(
                metrics_name=name,
                metrics_level=level,
                metric_sql_select=sql_select,
                error_text=error_message,
            )
            execute_query_str(conn, insert_error_record)

    def chunks(self, namespace_size: int) -> tuple:
        """
        Determine minimum and maximum position
        for slicing queries in chunks
        min_position is inclusive
        max_position is exclusive in determination of the chunk
        """

        # in case it is back filling, no chunks,
        # everything goes in one pass
        if self.chunk_no == 0 or self.number_of_tasks == 0:
            return 0, namespace_size

        chunk_size = math.ceil(namespace_size / self.number_of_tasks)

        max_position = chunk_size * self.chunk_no
        min_position = max_position - chunk_size

        return min_position, max_position

    def process(self, query_dict, connection) -> None:
        """
        Upload result of namespace ping to Snowflake
        """

        metric_name, metric_query, _ = self.get_prepared_values(query=query_dict)

        info(f"...Start loading metrics: {metric_name}")

        if "namespace_ultimate_parent_id" not in metric_query:
            info(f"Skipping ping {metric_name} due to lack of namespace information.")
            return

        self.upload(query_dict=query_dict, conn=connection)

        info(f"...End loading metric: {metric_name}")

    @staticmethod
    def get_query_count(queries: dict, run_backfill: bool = False) -> int:
        """
        Get number of metrics
        """
        run_type = "BACKFILL" if run_backfill else "REGULAR"
        query_count = len(queries)
        info(f"{run_type}: Will process: {query_count} queries")
        return query_count

    def calculate(self, queries: dict, run_backfill: bool = False) -> None:
        """
        Get the list of queries in json file format
        and execute it on Snowflake to calculate
        metrics
        """
        connection = self.engine_factory.connect()

        query_count = self.get_query_count(queries=queries, run_backfill=run_backfill)

        for i, query_dict in enumerate(queries):
            info(f"...Query counter: {i}/{query_count}")
            self.process(query_dict=query_dict, connection=connection)

        connection.close()
        self.engine_factory.dispose()

    def filter_backfill_queries(self, queries: List[dict]) -> List[dict]:
        """
        Filters queries based on what metrics were passed into DAG for backfill

        Args:
            queries: List of query dictionaries to filter

        Returns:
            List of filtered query dictionaries that match backfill criteria
        """
        return [
            query
            for query in queries
            if query.get("time_window_query")
            and query.get("counter_name") in self.metrics_to_backfill
        ]

    def read_namespace_queries(self, file_name: str) -> List[dict]:
        """
        Read in namespace query json file from the file system
        param file_name: str
        return: dict
        """
        full_path = os.path.join(os.path.dirname(__file__), file_name)

        return self.utils.load_from_json_file(file_name=full_path)

    def get_namespace_queries(self, run_backfill: bool):
        """
        Read in namespace file
        If backfilling, filter for only the metrics passed via Airflow
        Lastly, return only the query chunk corresponding to the Airflow task
        """
        namespace_queries = self.read_namespace_queries(
            file_name=self.utils.config[self.metrics_type]["source_file"]
        )

        if run_backfill:
            namespace_queries = self.filter_backfill_queries(namespace_queries)

        info(f"\nNumber of namespace_queries in total: {len(namespace_queries)}")
        start_slice, end_slice = self.chunks(namespace_size=len(namespace_queries))
        namespace_queries = namespace_queries[start_slice:end_slice]
        info(f"\nNumber of namespace_queries in this chunk: {len(namespace_queries)}")
        return namespace_queries

    def run(self, run_backfill: bool = False):
        """
        The main function in this module

        Does the following:
        1. If internal namespace, create the namespace query file
            - For database namespace, the namespace query file already exists in the repo
        2. read in the namespace query file
        3. Run each query in Snowflake and upload the results in a Snowflake table
        """

        info("<<<START PROCESSING>>>")

        if not run_backfill:
            info(f"number_of_tasks: {self.number_of_tasks}, chunk_no: {self.chunk_no}")
        info(f"metrics_type: {self.metrics_type}")

        if self.metrics_type == "internal_events_namespace_metrics":
            internal_events = InternalEventsNamespaceMetrics()
            # for internal_namespace create file that looks like database_namespace_queries.json
            internal_events.generate(is_backfill=run_backfill)

        namespace_queries = self.get_namespace_queries(run_backfill)

        self.calculate(
            queries=namespace_queries,
            run_backfill=run_backfill,
        )

        info("<<<END PROCESSING>>>")

    def backfill(self):
        """
        Routine to back-filling
        data for instance_namespace_metrics ping
        """
        self.run(run_backfill=True)


if __name__ == "__main__":
    basicConfig(stream=sys.stdout, level=20)
    Fire(NamespaceMetricsCalculation)
    info("Done with namespace pings.")
